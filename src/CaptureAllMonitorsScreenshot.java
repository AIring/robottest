import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

public class CaptureAllMonitorsScreenshot {
    public static void main(String[] args) {

            try {
                Rectangle virtualBounds = new Rectangle();

                GraphicsEnvironment ge = GraphicsEnvironment.getLocalGraphicsEnvironment();
                GraphicsDevice[] gs = ge.getScreenDevices();

                for (GraphicsDevice gd : gs) {
                    GraphicsConfiguration[] gc = gd.getConfigurations();

                    for (GraphicsConfiguration aGc : gc) {
                        virtualBounds = virtualBounds.union(aGc.getBounds());
                    }
                }

                Robot robot = new Robot();
                BufferedImage exportImage = robot.createScreenCapture(virtualBounds);

                File screenshotFile = new File("./AllMonitorsScreenshot-"+System.currentTimeMillis()+".png");
                ImageIO.write(exportImage, "png",screenshotFile);
                System.out.println("Screenshot successfully captured to '"+screenshotFile.getCanonicalPath()+"'!");

            } catch (AWTException | IOException ex) {
                ex.printStackTrace();
            }
    }
}
